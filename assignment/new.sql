-- Insert records into CUSTOMER table

INSERT INTO Customer VALUES (10000001, 'Brandon', 'Chong', 'chong@brandon.com', 4323541110, 'M', TO_DATE('1966-02-01', 'YYYY-MM-DD'), 'Apt 69', '38 Pigion St', 'Vienna', 'Burgenland', '56663', 'Austria', 6111876543234765);
INSERT INTO Customer VALUES (10000002, 'Jane', 'Smith', 'jane.smith@email.com', 4376543210, 'F', TO_DATE('1965-02-02', 'YYYY-MM-DD'), 'Apt 202', '456 Elm St', 'Vienna', 'Burgenland', '56789', 'Austria', NULL);
INSERT INTO Customer VALUES (10000003, 'Peter', 'Jones', 'peter.jones@email.com', 4385554444, 'M', TO_DATE('1964-03-03', 'YYYY-MM-DD'), 'Apt 303', '789 Oak St', 'Innsburk', 'Salzburg', '01234', 'Austria', NULL);
INSERT INTO Customer VALUES (10000004, 'Mary', 'Brown', 'mary.brown@email.com', 4372221111, 'F', TO_DATE('1973-04-04', 'YYYY-MM-DD'), 'Apt 404', '1011 Maple St', 'Linz', 'Styria', '98765', 'Austria', 1234567890123456);
INSERT INTO Customer VALUES (10000005, 'David', 'Williams', 'david.williams@email.com', 4361112222, 'M', TO_DATE('2000-05-05', 'YYYY-MM-DD'), 'Apt 505', '1234 Spruce St', 'Villach', 'Tyrol', '45678', 'Austria', 4321876543218765);
INSERT INTO Customer VALUES (10000006, 'Susan', 'Miller', 'susan.miller@email.com', 4233334444, 'F', TO_DATE('2003-06-06', 'YYYY-MM-DD'), 'Apt 606', '1456 Pine St', 'Graz', 'Basel', '12345', 'Switzerland', 5678123456781234);
INSERT INTO Customer VALUES (10000007, 'Michael', 'Davis', 'michael.davis@email.com', 4235556666, 'M', TO_DATE('2000-07-07', 'YYYY-MM-DD'), 'Apt 707', '1678 Walnut St', 'Chur', 'Freiburg', '56789', 'Switzerland', 1234567890123456);
INSERT INTO Customer VALUES (10000008, 'Sarah', 'Anderson', 'sarah.anderson@email.com', 4236667777, 'F', TO_DATE('1982-08-08', 'YYYY-MM-DD'), 'Apt 808', '1890 Birch St', 'Bern', 'Zug', '01234', 'Switzerland', 4321876543218765);
INSERT INTO Customer VALUES (10000009, 'Mark', 'Thomas', 'mark.thomas@email.com', 4237778888, 'M', TO_DATE('1961-09-09', 'YYYY-MM-DD'), 'Apt 909', '2101 Cedar St', 'Lugano', 'Schwyz', '98765', 'Switzerland', 5678123456781234);
INSERT INTO Customer VALUES (10000010, 'Patricia', 'Johnson', 'patricia.johnson@email.com', 4238889999, 'F', TO_DATE('1986-10-10', 'YYYY-MM-DD'), 'Apt 1010', '2312 Willow St', 'Winterth', 'Germany', '66695', 'Switzerland', NULL);
INSERT INTO Customer VALUES (10000011, 'Alice', 'Johnson', 'alice.j@email.com', 4976543210, 'F', TO_DATE('1985-05-12', 'YYYY-MM-DD'), 'Unit 205', '456 Oak St', 'Hamburg', 'Hamburg', '54321', 'Germany', 2345678901234567);
INSERT INTO Customer VALUES (10000012, 'Bob', 'Smith', 'bob.smith@email.com', 4951237890, 'M', TO_DATE('1982-09-23', 'YYYY-MM-DD'), 'Apt 3B', '789 Pine Ave', 'Munich', 'Bavaria', '67890', 'Germany', 3456789012345678);
INSERT INTO Customer VALUES (10000013, 'Emily', 'White', 'emily.white@email.com', 4912223333, 'F', TO_DATE('1995-12-07', 'YYYY-MM-DD'), 'Suite 10', '101 Elm St', 'Leipzig', 'Saxony', '11223', 'Germany', 4567890123456789);
INSERT INTO Customer VALUES (10000014, 'Sophia', 'Davis', 'sophia.d@email.com', 4974445555, 'F', TO_DATE('1993-07-28', 'YYYY-MM-DD'), 'Unit 7', '789 Maple Ln', 'Wiesbaden', 'Hesse', '88888', 'Germany', 6789012345678901);
INSERT INTO Customer VALUES (10000015, 'Olivia', 'Martinez', 'olivia.m@email.com', 4965553333, 'F', TO_DATE('1978-04-18', 'YYYY-MM-DD'), 'Suite 23', '333 Pine Ln', 'Bremerhaven', 'Bremen', '44444', 'Germany', 8901234567890123);




Sure, let's look at some examples of values that can be represented by FLOAT(4) and FLOAT(10) in Oracle Database. Remember that the precision specifies the number of binary digits used to represent the floating-point number. We'll convert these binary representations to decimal for clarity:

### Example for FLOAT(4):

For FLOAT(4), the precision is 4, so the range of values is from \(2^{-3}\) to \(2^{0}\) in decimal notation.

1. Binary: 0010, Decimal: 0.5
2. Binary: 0100, Decimal: 1.0
3. Binary: 1000, Decimal: -2.0 (assuming signed)

### Example for FLOAT(10):

For FLOAT(10), the precision is 10, so the range of values is from \(2^{-9}\) to \(2^{0}\) in decimal notation.

1. Binary: 0000000010, Decimal: 0.25
2. Binary: 0000000100, Decimal: 0.5
3. Binary: 1000000000, Decimal: -2.0 (assuming signed)

These examples demonstrate different binary representations within the specified precision ranges for FLOAT(4) and FLOAT(10). Keep in mind that these are just a few examples, and there are many more possible values within the specified ranges for each precision. Also, the actual stored values may vary due to the limitations of floating-point representation.