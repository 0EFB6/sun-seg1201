-- Create Customer Entity
CREATE TABLE Customer (
    c_id NUMBER PRIMARY KEY,
    c_fname VARCHAR2(50),
    c_lname VARCHAR2(50),
    c_email VARCHAR2(100),
    c_hp_number VARCHAR2(20),
    c_gender VARCHAR2(10),
    c_dob DATE,
    c_unit VARCHAR2(20),
    c_street_address VARCHAR2(100),
    c_city VARCHAR2(50),
    c_state VARCHAR2(50),
    c_zip_code VARCHAR2(20),
    c_country VARCHAR2(50),
    c_credit_card_number VARCHAR2(20)
);

-- Create Account Entity
CREATE TABLE Account (
    acc_id NUMBER PRIMARY KEY,
    c_id NUMBER,
    acc_username VARCHAR2(50),
    acc_password VARCHAR2(50),
    acc_creation_date DATE,
    acc_last_login DATE,
    CONSTRAINT fk_account_customer FOREIGN KEY (c_id) REFERENCES Customer(c_id)
);

-- Create Vendor Entity
CREATE TABLE Vendor (
    v_id NUMBER PRIMARY KEY,
    v_name VARCHAR2(100),
    v_contact VARCHAR2(50),
    v_hq_address VARCHAR2(100),
    v_founded_by VARCHAR2(50),
    v_advocacy VARCHAR2(100),
    v_core_values VARCHAR2(100),
    v_awards VARCHAR2(100)
);

-- Create Product Entity
CREATE TABLE Product (
    p_id NUMBER PRIMARY KEY,
    v_id NUMBER,
    p_name VARCHAR2(100),
    p_description VARCHAR2(255),
    p_upc VARCHAR2(20),
    p_weight NUMBER,
    p_unit_price NUMBER,
    p_category VARCHAR2(50),
    p_expiration_date DATE,
    CONSTRAINT fk_product_vendor FOREIGN KEY (v_id) REFERENCES Vendor(v_id)
);

-- Create Order Entity
CREATE TABLE OrderEntity (
    o_id NUMBER PRIMARY KEY,
    c_id NUMBER,
    o_date DATE,
    o_status VARCHAR2(20),
    CONSTRAINT fk_order_customer FOREIGN KEY (c_id) REFERENCES Customer(c_id)
);

-- Create Orderline Entity
CREATE TABLE Orderline (
    o_id NUMBER,
    p_id NUMBER,
    p_name VARCHAR2(100),
    p_description VARCHAR2(255),
    p_unit_price NUMBER,
    ol_quantity NUMBER,
    PRIMARY KEY (o_id, p_id),
    CONSTRAINT fk_orderline_order FOREIGN KEY (o_id) REFERENCES OrderEntity(o_id),
    CONSTRAINT fk_orderline_product FOREIGN KEY (p_id, p_name, p_description, p_unit_price)
        REFERENCES Product(p_id, p_name, p_description, p_unit_price)
);

-- Create Rating Entity
CREATE TABLE Rating (
    rating_id NUMBER PRIMARY KEY,
    c_id NUMBER,
    p_id NUMBER,
    rating_value NUMBER,
    rating_date DATE,
    CONSTRAINT fk_rating_customer FOREIGN KEY (c_id) REFERENCES Customer(c_id),
    CONSTRAINT fk_rating_product FOREIGN KEY (p_id) REFERENCES Product(p_id)
);

-- Create Referral Entity
CREATE TABLE Referral (
    referral_id NUMBER PRIMARY KEY,
    referrer_c_id NUMBER,
    referred_c_id NUMBER,
    referral_code VARCHAR2(20),
    CONSTRAINT fk_referral_referrer FOREIGN KEY (referrer_c_id) REFERENCES Customer(c_id),
    CONSTRAINT fk_referral_referred FOREIGN KEY (referred_c_id) REFERENCES Customer(c_id)
);

-- Create Credit Entity
CREATE TABLE Credit (
    c_id NUMBER,
    credit NUMBER,
    PRIMARY KEY (c_id, credit),
    CONSTRAINT fk_credit_customer FOREIGN KEY (c_id) REFERENCES Customer(c_id)
);

-- Create Shipment Entity
CREATE TABLE Shipment (
    s_id NUMBER PRIMARY KEY,
    o_id NUMBER,
    s_method VARCHAR2(50),
    s_courier VARCHAR2(50),
    s_cost NUMBER,
    s_status VARCHAR2(20),
    CONSTRAINT fk_shipment_order FOREIGN KEY (o_id) REFERENCES OrderEntity(o_id)
);

-- Create Payment Entity
CREATE TABLE Payment (
    TransactionID NUMBER PRIMARY KEY,
    o_id NUMBER,
    PaymentMethod VARCHAR2(50),
    CONSTRAINT fk_payment_order FOREIGN KEY (o_id) REFERENCES OrderEntity(o_id)
);

-- Create Order Review Entity
CREATE TABLE OrderReview (
    or_id NUMBER PRIMARY KEY,
    o_id NUMBER,
    or_description VARCHAR2(255),
    or_date DATE,
    CONSTRAINT fk_order_review_order FOREIGN KEY (o_id) REFERENCES OrderEntity(o_id)
);


INSERT INTO Customer VALUES (1, 'John', 'Doe', 'john.doe@email.com', '123-456-7890', 'M', TO_DATE('1990-01-01', 'YYYY-MM-DD'), 'Apt 101', '123 Main St', 'City', 'State', '12345', 'Country', '1234-5678-9012-3456');
INSERT INTO Vendor VALUES (1, 'Vendor1', 'Contact1', 'HQ Address1', 'Founder1', 'Advocacy1', 'Core Values1', 'Awards1');
INSERT INTO Product VALUES (1, 1, 'Product1', 'Description1', 'UPC1', 1.5, 10.99, 'Category1', TO_DATE('2024-01-01', 'YYYY-MM-DD'));
INSERT INTO OrderEntity VALUES (1, 1, TO_DATE('2023-12-01', 'YYYY-MM-DD'), 'Shipped');
INSERT INTO Orderline VALUES (1, 1, 'Product1', 'Description1', 10.99, 2);
INSERT INTO Rating VALUES (1, 1, 1, 5, TO_DATE('2023-12-02', 'YYYY-MM-DD'));
INSERT INTO Referral VALUES (1, 1, 2, 'REF123');
INSERT INTO Credit VALUES (1, 100);
INSERT INTO Shipment VALUES (1, 1, 'Standard', 'Courier1', 5.99, 'Delivered');
INSERT INTO Payment VALUES (1, 1, 'Credit Card');
INSERT INTO OrderReview VALUES (1, 1, 'Great service!', TO_DATE('2023-12-03', 'YYYY-MM-DD'));