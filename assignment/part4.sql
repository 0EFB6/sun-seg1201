-- Q1

SELECT 
    c.c_id,
    CONCAT(c.c_fname, CONCAT(' ', c.c_lname)) AS C_NAME,
    (SELECT COUNT(*) FROM C_ORDER o WHERE o.c_id = c.c_id AND o_date > ADD_MONTHS(CURRENT_DATE, -3)) AS total_orders_in_past_3_months,
    (SELECT SUM(oa.grand_total) AS total_spending FROM ORDER_ALL oa, C_Order o
     WHERE o.o_id = oa.order_id AND o.c_id = c.c_id AND o_date > ADD_MONTHS(CURRENT_DATE, -3)) AS total_spending_in_past_3_months,
    AVG(r.rating_value) AS average_rating
FROM CUSTOMER c INNER JOIN RATING r ON c.c_id = r.c_id
GROUP BY c.c_id, c.c_fname, c.c_lname
HAVING AVG(r.rating_value) >= 3
ORDER BY total_orders_in_past_3_months DESC, average_rating DESC;

-- Q2

SELECT
    p.p_name AS herb_name,
    v.v_name AS vendor_name,
    SUM(ol.ol_quantity) AS quantity_sold
FROM CUSTOMER c
INNER JOIN C_ORDER o ON c.c_id = o.c_id
INNER JOIN ORDERLINE ol ON o.o_id = ol.o_id
INNER JOIN PRODUCT p ON ol.p_id = p.p_id
INNER JOIN VENDOR v ON p.v_id = v.v_id
WHERE c.c_dob < ADD_MONTHS(SYSDATE, -480) AND UPPER(p.p_name) LIKE '%HERBS%'
GROUP BY p.p_name, v.v_name
HAVING SUM(ol.ol_quantity) >= 5
ORDER BY quantity_sold DESC;

-- Q3

SELECT  
    ORR.or_id AS ORDER_REVIEW_ID, 
    ORR.o_id AS ORDER_ID, 
    ORR.or_description AS ORDER_REVIEW_DESCRIPTION,  
    s.s_courier AS COURIER, 
    COALESCE(CO.o_status, 'Not Found') AS order_status  
FROM SHIPMENT s 
RIGHT OUTER JOIN ORDER_REVIEW ORR ON S.o_id = ORR.o_id  
LEFT JOIN C_ORDER CO ON ORR.o_id = CO.o_id  
WHERE ORR.or_description LIKE '%shipping%' OR ORR.or_description LIKE '%shipment%' OR ORR.or_description LIKE '%delivery%';

-- Q4

-- Customers who spent less than $600 and have more than two referrals 
SELECT 
  c.c_id AS customer_id, 
  c.c_fname AS first_name,
  c.c_lname AS last_name, 
  SUM(ol.ol_quantity * p.p_unit_price) AS total_spent, 
  COUNT(DISTINCT r.referral_id) AS total_referrals 
FROM CUSTOMER c 
JOIN C_ORDER o ON c.c_id = o.c_id 
JOIN ORDERLINE ol ON o.o_id = ol.o_id 
JOIN PRODUCT p ON ol.p_id = p.p_id 
LEFT JOIN REFERRAL r ON c.c_id = r.referrer_c_id 
GROUP BY c.c_id, c.c_fname, c.c_lname 
HAVING SUM(ol.ol_quantity * p.p_unit_price) < 600 AND COUNT(DISTINCT r.referral_id) > 1 
MINUS 

-- Customers who spent more than $600 
SELECT
  c.c_id AS customer_id, 
  c.c_fname AS first_name,
  c.c_lname AS last_name, 
  SUM(ol.ol_quantity * p.p_unit_price) AS total_spent, 
  COUNT(DISTINCT r.referral_id) AS total_referrals 
FROM CUSTOMER c 
JOIN C_ORDER o ON c.c_id = o.c_id 
JOIN ORDERLINE ol ON o.o_id = ol.o_id 
JOIN PRODUCT p ON ol.p_id = p.p_id 
LEFT JOIN REFERRAL r ON c.c_id = r.referrer_c_id 
GROUP BY c.c_id, c.c_fname, c.c_lname 
HAVING SUM(ol.ol_quantity * p.p_unit_price) > 600;

-- Q5

WITH OrderedOrders AS (
  SELECT
    ORDER_ID,
    CUSTOMER_ID,
    c.c_fname,
    c.c_lname,
    GRAND_TOTAL_BEFORE_CREDIT,
    GRAND_TOTAL,
    ROW_NUMBER() OVER (PARTITION BY c.c_id ORDER BY ORDER_ID DESC) AS OrderRank
  FROM ORDER_ALL oa
  JOIN CUSTOMER c ON oa.customer_id = c.c_id
  WHERE c.c_fname || ' ' || c.c_lname = 'Sally Smith'
)
SELECT
  ORDER_ID,
  CUSTOMER_ID,
  c_fname,
  c_lname,
  GRAND_TOTAL_BEFORE_CREDIT,
  GRAND_TOTAL
FROM OrderedOrders
WHERE OrderRank = 1;