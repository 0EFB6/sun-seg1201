-- SEG1201 Database Fundamentals - Final Assessment - Group 5

-- Set date format

ALTER SESSION SET NLS_DATE_FORMAT='DD/MM/YYYY';

-- Drop the tables

DROP TABLE CUSTOMER CASCADE CONSTRAINTS;
DROP TABLE VENDOR CASCADE CONSTRAINTS;
DROP TABLE PRODUCT CASCADE CONSTRAINTS;
DROP TABLE REFERRAL_CODE CASCADE CONSTRAINTS;
DROP TABLE C_ORDER CASCADE CONSTRAINTS;
DROP TABLE ORDERLINE CASCADE CONSTRAINTS;
DROP TABLE RATING CASCADE CONSTRAINTS;
DROP TABLE REFERRAL CASCADE CONSTRAINTS;
DROP TABLE SHIPMENT CASCADE CONSTRAINTS;
DROP TABLE PAYMENT CASCADE CONSTRAINTS;
DROP TABLE ORDER_REVIEW CASCADE CONSTRAINTS;

-- Create the tables

CREATE TABLE CUSTOMER (
  c_id NUMBER(8) PRIMARY KEY,
  c_fname VARCHAR2(50) NOT NULL,
  c_lname VARCHAR2(50) NOT NULL,
  c_email VARCHAR2(100) NOT NULL,
  c_hp_number NUMBER(15) NOT NULL,
  c_gender CHAR(1) NOT NULL CHECK (c_gender IN ('M', 'F')),
  c_dob DATE NOT NULL,
  c_unit VARCHAR2(20) NOT NULL,
  c_street_address VARCHAR2(100) NOT NULL,
  c_city VARCHAR2(30) NOT NULL,
  c_state VARCHAR2(50) NOT NULL,
  c_zip_code VARCHAR2(10) NOT NULL,
  c_country VARCHAR2(50) NOT NULL,
  c_credit_card_number NUMBER(16,0)
);

CREATE TABLE VENDOR (
  v_id NUMBER(10) PRIMARY KEY,
  v_name VARCHAR2(255) NOT NULL,
  v_contact VARCHAR2(50) NOT NULL,
  v_hq_address VARCHAR2(255) NOT NULL,
  v_founded_by VARCHAR2(60) NOT NULL,
  v_advocacy VARCHAR2(255) NOT NULL,
  v_core_values VARCHAR2(60) NOT NULL,
  v_awards VARCHAR2(255) NOT NULL
);

CREATE TABLE PRODUCT (
  p_id NUMBER(6) PRIMARY KEY,
  v_id NUMBER(10) NOT NULL,
  p_name VARCHAR2(100) NOT NULL,
  p_description VARCHAR2(1000) NOT NULL,
  p_upc NUMBER(12) UNIQUE NOT NULL,
  p_weight_kg FLOAT(4) NOT NULL,
  p_unit_price NUMBER(5,2) NOT NULL,
  p_category VARCHAR2(18) NOT NULL CHECK (p_category IN ('supplements', 'health and beauty', 'sports nutrition', 'essential oils', 'pet health')),
  p_expiration_date DATE NOT NULL,
  FOREIGN KEY (v_id) REFERENCES VENDOR(v_id)
);

CREATE TABLE REFERRAL_CODE (
  referral_code VARCHAR2(8) PRIMARY KEY,
  c_id NUMBER(8) UNIQUE,  
  FOREIGN KEY (c_id) REFERENCES CUSTOMER(c_id)   
);

CREATE TABLE REFERRAL (
  referral_id NUMBER(10) PRIMARY KEY,
  referrer_c_id NUMBER(8) NOT NULL,
  referred_c_id NUMBER(8),
  referral_code VARCHAR2(8) NOT NULL,
  FOREIGN KEY (referrer_c_id) REFERENCES CUSTOMER(c_id),
  FOREIGN KEY (referred_c_id) REFERENCES CUSTOMER(c_id)
);

CREATE TABLE C_ORDER (
  o_id NUMBER(10) PRIMARY KEY,
  c_id NUMBER(8) NOT NULL,
  is_first_and_second_time_purchase CHAR(1),
  last_o_id NUMBER(10) UNIQUE,
  last_o_date TIMESTAMP(0),
  referral_code VARCHAR2(8),
  o_date TIMESTAMP(0) NOT NULL,
  o_status VARCHAR2(8) NOT NULL CHECK (o_status IN ('Shipped', 'Shipping', 'Packing')),
  FOREIGN KEY (c_id) REFERENCES CUSTOMER(c_id),
  FOREIGN KEY (referral_code) REFERENCES REFERRAL_CODE(referral_code)
);

CREATE TABLE ORDERLINE (
  ol_id NUMBER(12) PRIMARY KEY,
  o_id NUMBER(10) NOT NULL,
  p_id NUMBER(6) NOT NULL,
  ol_quantity NUMBER(3) NOT NULL CHECK (ol_quantity > 0),
  FOREIGN KEY (o_id) REFERENCES C_ORDER(o_id),
  FOREIGN KEY (p_id) REFERENCES PRODUCT(p_id)
);

CREATE TABLE RATING (
  rating_id NUMBER(8) PRIMARY KEY,
  c_id NUMBER(8) NOT NULL,
  p_id NUMBER(6) NOT NULL,
  rating_value NUMBER(1) NOT NULL CHECK (rating_value > 0 AND rating_value <= 5),
  rating_date DATE NOT NULL,
  FOREIGN KEY (c_id) REFERENCES CUSTOMER(c_id),
  FOREIGN KEY (p_id) REFERENCES PRODUCT(p_id)
);

CREATE TABLE SHIPMENT (
    s_id NUMBER(10) PRIMARY KEY,
    o_id NUMBER(10) NOT NULL,
    s_method VARCHAR2(10) NOT NULL CHECK (s_method IN ('Car', 'Motorcycle', 'Air', 'Ship', 'Lorry')),
    s_courier VARCHAR2(20) NOT NULL,
    s_cost FLOAT(10) NOT NULL,
    s_status VARCHAR2(15) NOT NULL CHECK (s_status IN ('Delivered', 'Awaiting Pickup', 'Packing')),
    FOREIGN KEY (o_id) REFERENCES C_ORDER(o_id)
);

CREATE TABLE PAYMENT (
    transaction_id NUMBER(11) PRIMARY KEY,
    o_id NUMBER(10) NOT NULL,
    payment_method VARCHAR2(20) NOT NULL,
	FOREIGN KEY (o_id) REFERENCES C_ORDER(o_id)
);

CREATE TABLE ORDER_REVIEW (
    or_id NUMBER(10) PRIMARY KEY,
    o_id NUMBER(10) NOT NULL,
    or_description VARCHAR2(1000) NOT NULL,
    or_date DATE NOT NULL,
    FOREIGN KEY (o_id) REFERENCES C_ORDER(o_id)
);

-- Insert records into CUSTOMER table

INSERT INTO Customer VALUES (10000001, 'Sally', 'Smith', 'chong@brandon.com', 4323541110, 'M', TO_DATE('1966-02-01', 'YYYY-MM-DD'), 'Apt 69', '38 Pigion St', 'Vienna', 'Burgenland', '56663', 'Austria', NULL);
INSERT INTO Customer VALUES (10000002, 'Jane', 'Smith', 'jane.smith@email.com', 4376543210, 'F', TO_DATE('1965-02-02', 'YYYY-MM-DD'), 'Apt 202', '456 Elm St', 'Vienna', 'Burgenland', '56789', 'Austria', NULL);
INSERT INTO Customer VALUES (10000003, 'Peter', 'Jones', 'peter.jones@email.com', 4385554444, 'M', TO_DATE('1964-03-03', 'YYYY-MM-DD'), 'Apt 303', '789 Oak St', 'Innsburk', 'Salzburg', '01234', 'Austria', NULL);
INSERT INTO Customer VALUES (10000004, 'Mary', 'Brown', 'mary.brown@email.com', 4372221111, 'F', TO_DATE('1973-04-04', 'YYYY-MM-DD'), 'Apt 404', '1011 Maple St', 'Linz', 'Styria', '98765', 'Austria', 1234567890123456);
INSERT INTO Customer VALUES (10000005, 'David', 'Williams', 'david.williams@email.com', 4361112222, 'M', TO_DATE('2000-05-05', 'YYYY-MM-DD'), 'Apt 505', '1234 Spruce St', 'Villach', 'Tyrol', '45678', 'Austria', 4321876543218765);
INSERT INTO Customer VALUES (10000006, 'Susan', 'Miller', 'susan.miller@email.com', 4233334444, 'F', TO_DATE('2003-06-06', 'YYYY-MM-DD'), 'Apt 606', '1456 Pine St', 'Graz', 'Basel', '12345', 'Switzerland', NULL);
INSERT INTO Customer VALUES (10000007, 'Michael', 'Davis', 'michael.davis@email.com', 4235556666, 'M', TO_DATE('2000-07-07', 'YYYY-MM-DD'), 'Apt 707', '1678 Walnut St', 'Chur', 'Freiburg', '56789', 'Switzerland', NULL);
INSERT INTO Customer VALUES (10000008, 'Sarah', 'Anderson', 'sarah.anderson@email.com', 4236667777, 'F', TO_DATE('1982-08-08', 'YYYY-MM-DD'), 'Apt 808', '1890 Birch St', 'Bern', 'Zug', '01234', 'Switzerland', 4321876543218765);
INSERT INTO Customer VALUES (10000009, 'Mark', 'Thomas', 'mark.thomas@email.com', 4237778888, 'M', TO_DATE('1961-09-09', 'YYYY-MM-DD'), 'Apt 909', '2101 Cedar St', 'Lugano', 'Schwyz', '98765', 'Switzerland', NULL);
INSERT INTO Customer VALUES (10000010, 'Patricia', 'Johnson', 'patricia.johnson@email.com', 4238889999, 'F', TO_DATE('1986-10-10', 'YYYY-MM-DD'), 'Apt 1010', '2312 Willow St', 'Winterth', 'Germany', '66695', 'Switzerland', 1234567855669000);
INSERT INTO Customer VALUES (10000011, 'Alice', 'Johnson', 'alice.j@email.com', 4976543210, 'F', TO_DATE('1985-05-12', 'YYYY-MM-DD'), 'Unit 205', '456 Oak St', 'Hamburg', 'Hamburg', '54321', 'Germany', NULL);
INSERT INTO Customer VALUES (10000012, 'Bob', 'Smith', 'bob.smith@email.com', 4951237890, 'M', TO_DATE('1982-09-23', 'YYYY-MM-DD'), 'Apt 3B', '789 Pine Ave', 'Munich', 'Bavaria', '67890', 'Germany', 3456789012345678);
INSERT INTO Customer VALUES (10000013, 'Emily', 'White', 'emily.white@email.com', 4912223333, 'F', TO_DATE('1995-12-07', 'YYYY-MM-DD'), 'Suite 10', '101 Elm St', 'Leipzig', 'Saxony', '11223', 'Germany', NULL);
INSERT INTO Customer VALUES (10000014, 'Sophia', 'Davis', 'sophia.d@email.com', 4974445555, 'F', TO_DATE('1993-07-28', 'YYYY-MM-DD'), 'Unit 7', '789 Maple Ln', 'Wiesbaden', 'Hesse', '88888', 'Germany', NULL);
INSERT INTO Customer VALUES (10000015, 'Olivia', 'Martinez', 'olivia.m@email.com', 4965553333, 'F', TO_DATE('1978-04-18', 'YYYY-MM-DD'), 'Suite 23', '333 Pine Ln', 'Bremerhaven', 'Bremen', '44444', 'Germany', NULL);

-- Insert records into VENDOR table

INSERT INTO Vendor VALUES (9066696901, 'TechSolutions', '1234567890', '123 Tech Street, Innovation City, California, United States', 'John Doe', 'Environmental Sustainability', 'Innovation, Integrity, Collaboration', 'Best Tech Company 2022, Green Initiative Award');
INSERT INTO Vendor VALUES (9066696902, 'BioHealth Labs', '9876543210', '456 Health Avenue, Biocity, California, United States', 'Jane Smith', 'Health and Wellness', 'Quality, Research, Community', 'Healthcare Excellence Award 2023, Research Innovation Prize');
INSERT INTO Vendor VALUES (9066696903, 'EcoCraft', '5555555555', '789 Green Lane, EcoVille, California, United States', 'David Green', 'Environmental Conservation', 'Sustainability, Eco-friendly, Responsibility', 'Green Business of the Year 2021, EcoLeadership Award');
INSERT INTO Vendor VALUES (9066696904, 'InnoTech Solutions', '8888888888', '321 Innovation Road, TechHub, California, United States', 'Michael Charlie', 'Innovation and Technology', 'Creativity, Precision, Progress', 'Tech Innovation Award 2022, Excellence in Technology Solutions');
INSERT INTO Vendor VALUES (9066696905, 'GlobalGoods Trading', '1111111111', '567 Trade Center, GlobalCity, California, United States', 'Emily Global', 'Fair Trade and Ethical Practices', 'Fairness, Integrity, Global Citizenship', 'Ethical Business Award 2023, Fair Trade Certification');
INSERT INTO Vendor VALUES (9066696906, 'ArtisanCraft Creations', '9999999999', '654 Art Street, Creativity Town, California, United States', 'Alex Artisan', 'Supporting Artisan Communities', 'Creativity, Craftsmanship, Community', 'Artisan Excellence Award 2022, Community Impact Prize');
INSERT INTO Vendor VALUES (9066696907, 'PetHealth Labs', '2222222222', '987 Pet Street, PetVille, California, United States', 'Peter Pet', 'Pet Health and Wellness', 'Wealthy Is Key', 'Pet Health Excellence Award 2023, Research Innovation Prize');
INSERT INTO Vendor VALUES (9066696908, 'PetCraft Creations', 'petcraft@creation.com', '789 Pet Street, PetVille, California, United States', 'Peter Creg', 'Supporting Artisan Communities', 'Community Where Pet Meets', 'Pet Artisan Excellence Award 2022, Community Impact Prize');
INSERT INTO Vendor VALUES (9066696909, 'BeautyHealth Labs', '4444444444', '654 Beauty Street, BeautyVille, California, United States', 'Jane Beauty', 'Beauty and Wellness', 'Beautifying Us', 'Beauty Health Excellence Award 2023, Research Innovation Prize');
INSERT INTO Vendor VALUES (9066696910, 'BeautyCraft Creations', '5555555555', '987 Beauty Street, BeautyVille, California, United States', 'Joanne Beauty', 'Supporting Artisan Communities', 'Creativity& Craftsmanship', 'Beauty Artisan Excellence Award 2022, Community Impact Prize');
INSERT INTO Vendor VALUES (9066696911, 'SportsHealth Labs', 'sport@labs.com', '654 Sports Street, SportsVille, California, United States', 'David Sports', 'Sports and Wellness', 'Health and Research', 'Sports Health Excellence Award 2023, Research Innovation Prize');
INSERT INTO Vendor VALUES (9066696912, 'SportsCraft Creations', 'craft@sport.com', '987 Sports Street, SportsVille, California, United States', 'Charlie Sports', 'Supporting Artisan Communities', 'Where Sports Meet Craftsmanship', 'Sports Artisan Excellence Award 2022, Community Impact Prize');
INSERT INTO Vendor VALUES (9066696913, 'EssentialHealth Labs', '8888888888', '654 Essential Street, EssentialVille, California, United States', 'Michael Essential', 'Essential Oils and Wellness', 'Essential Health', 'Essential Health Excellence Award 2023, Research Innovation Prize');
INSERT INTO Vendor VALUES (9066696914, 'EssentialCraft Creations', 'essentialcreation@creation', '987 Essential Street, EssentialVille, California, United States', 'Minleg Essential', 'Supporting Artisan Communities', 'Craftsmanship and Community', 'Essential Artisan Excellence Award 2022, Community Impact Prize');
INSERT INTO Vendor VALUES (9066696915, 'NutritionHealth Labs', '1111111111', '654 Nutrition Street, NutritionVille, California, United States', 'Emily Nutrition', 'Nutrition and Wellness', 'Nutritious Life', 'Nutrition Health Excellence Award 2023, Research Innovation Prize');

-- Insert records into PRODUCT table

INSERT INTO Product VALUES (777001, 9066696901, 'Turmeric Capsules Herbs', 'Organic turmeric capsules with black pepper for better absorption', 123456789012, 0.05, 19.50, 'supplements', TO_DATE('2025-12-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777002, 9066696901, 'Collagen Beauty Elixir', 'Premium collagen powder for radiant skin and hair', 234567890123, 0.1, 29.50, 'health and beauty', TO_DATE('2024-11-30', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777003, 9066696902, 'Bone Broth Protein Herbs', 'Grass-fed bone broth powder for joint support', 345678901234, 0.08, 24.50, 'sports nutrition', TO_DATE('2024-10-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777004, 9066696902, 'Lavender Essential Oil Herbs', 'Pure lavender essential oil for relaxation and aromatherapy', 456789012345, 0.02, 14.50, 'essential oils', TO_DATE('2024-09-30', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777005, 9066696903, 'Omega-3 Fish Oil', 'High-potency fish oil capsules for heart health', 567890123456, 0.03, 21.50, 'supplements', TO_DATE('2024-08-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777006, 9066696903, 'Vitamin C Serum', 'Antioxidant-rich vitamin C serum for skin brightening', 678901234567, 0.04, 27.50, 'health and beauty', TO_DATE('2024-07-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777007, 9066696904, 'Joint Support Chewables Herbs', 'Tasty chewable tablets for pet joint health', 789012345678, 0.15, 16.50, 'pet health', TO_DATE('2025-06-30', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777008, 9066696904, 'Catnip Infused Toys', 'Interactive cat toys with organic catnip for playtime', 890123456789, 0.2, 9.50, 'pet health', TO_DATE('2024-05-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777009, 9066696905, 'Eucalyptus Shower Gel', 'Invigorating shower gel with eucalyptus essential oil', 901234567890, 0.25, 12.50, 'health and beauty', TO_DATE('2024-04-30', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777010, 9066696905, 'Protein Bars Variety Pack Herbs', 'Assorted protein bars for a quick and healthy snack', 123456789011, 0.18, 19.00, 'sports nutrition', TO_DATE('2024-03-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777011, 9066696906, 'Lavender Essential Oil 2.0 Herbs', 'Enhanced Version of Pure lavender essential oil for relaxation and aromatherapy', 234567890122, 0.02, 19.00, 'essential oils', TO_DATE('2024-06-28', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777012, 9066696907, 'Vitamin A & C Serum', 'Antioxidant-rich vitamin A & C serum for skin brightening', 345678901233, 0.05, 29.00, 'health and beauty', TO_DATE('2024-10-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777013, 9066696908, 'Joint Support Pills Herbs', 'Tasty pills for pet joint health', 456789012344, 0.09, 12.00, 'pet health', TO_DATE('2024-03-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777014, 9066696909, 'Catnip Infused Toys 2.0', 'Interactive cat toys with organic catnip for playtime', 567890123455, 0.2, 9.00, 'pet health', TO_DATE('2024-05-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777015, 9066696910, 'Eucalyptus Shower Gel 2.0', 'Ehanced Version of Invigorating shower gel with eucalyptus essential oil', 678901234566, 0.23, 15.00, 'health and beauty', TO_DATE('2024-04-30', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777016, 9066696911, 'Protein Bars Variety Pack Herbs 2.0', 'Large Portion of Assorted protein bars for a quick and healthy snack', 789012345677, 0.38, 39.00, 'sports nutrition', TO_DATE('2024-03-21', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777017, 9066696912, 'Turmeric Capsules Herbs 2.0', 'Enhanced Version of Organic turmeric capsules with black pepper for better absorption', 890123456788, 0.15, 33.00, 'supplements', TO_DATE('2025-10-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777018, 9066696913, 'Collagen Beauty Elixir 2.0', 'Enhanced Version of Premium collagen powder for radiant skin and hair', 901234567899, 0.3, 39.00, 'health and beauty', TO_DATE('2024-06-30', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777019, 9066696914, 'Workout Protein', 'Protein powder for working out', 123456789099, 0.38, 46.00, 'sports nutrition', TO_DATE('2024-05-31', 'YYYY-MM-DD'));
INSERT INTO Product VALUES (777020, 9066696915, 'Workout Protein Chocolate Flavour', 'Chocolate powder protein powder for working out', 123456789019, 0.20, 21.00, 'sports nutrition', TO_DATE('2024-05-31', 'YYYY-MM-DD'));

-- Insert records into REFERRAL_CODE table

INSERT INTO Referral_code VALUES ('REF123', 10000001);
INSERT INTO Referral_code VALUES ('REF121', 10000002);
INSERT INTO Referral_code VALUES ('REF126', 10000003);
INSERT INTO Referral_code VALUES ('REF128', 10000004);
INSERT INTO Referral_code VALUES ('REF129', 10000005);
INSERT INTO Referral_code VALUES ('REF120', 10000006);
INSERT INTO Referral_code VALUES ('REF125', 10000007);
INSERT INTO Referral_code VALUES ('REF122', 10000008);
INSERT INTO Referral_code VALUES ('REF124', 10000009);
INSERT INTO Referral_code VALUES ('REF134', 10000010);
INSERT INTO Referral_code VALUES ('REF131', 10000011);
INSERT INTO Referral_code VALUES ('REF135', 10000012);
INSERT INTO Referral_code VALUES ('REF136', 10000013);
INSERT INTO Referral_code VALUES ('REF137', 10000014);
INSERT INTO Referral_code VALUES ('REF138', 10000015);

-- Insert records into REFERRAL table

INSERT INTO Referral VALUES (9870005551, 10000001, NULL, 'REF123');
INSERT INTO Referral VALUES (9870005552, 10000002, 10000003, 'REF121');
INSERT INTO Referral VALUES (9870005553, 10000002, 10000006, 'REF121');
INSERT INTO Referral VALUES (9870005554, 10000003, 10000005, 'REF126');
INSERT INTO Referral VALUES (9870005555, 10000003, 10000009, 'REF126');
INSERT INTO Referral VALUES (9870005556, 10000004, NULL, 'REF128');
INSERT INTO Referral VALUES (9870005557, 10000005, NULL, 'REF129');
INSERT INTO Referral VALUES (9870005558, 10000006, NULL, 'REF120');
INSERT INTO Referral VALUES (9870005559, 10000006, 10000013, 'REF120');
INSERT INTO Referral VALUES (9870005560, 10000007, NULL, 'REF125');
INSERT INTO Referral VALUES (9870005561, 10000008, NULL, 'REF122');
INSERT INTO Referral VALUES (9870005562, 10000009, NULL, 'REF124');
INSERT INTO Referral VALUES (9870005563, 10000010, 10000011, 'REF134');
INSERT INTO Referral VALUES (9870005564, 10000011, NULL, 'REF131');
INSERT INTO Referral VALUES (9870005565, 10000012, NULL, 'REF135');
INSERT INTO Referral VALUES (9870005566, 10000013, NULL, 'REF136');
INSERT INTO Referral VALUES (9870005567, 10000014, 10000015, 'REF137');
INSERT INTO Referral VALUES (9870005568, 10000015, NULL, 'REF138');

-- Insert records into C_ORDER table

INSERT INTO C_Order VALUES (3366009901, 10000001, 'Y', NULL, NULL, NULL, TO_TIMESTAMP('2023-11-01 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipping');
INSERT INTO C_Order VALUES (3366009902, 10000002, 'Y', NULL, NULL, NULL, TO_TIMESTAMP('2023-11-02 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipping');
INSERT INTO C_Order VALUES (3366009903, 10000003, 'Y', NULL, NULL, 'REF121', TO_TIMESTAMP('2023-11-03 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009904, 10000004, 'Y', NULL, NULL, NULL, TO_TIMESTAMP('2023-11-04 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009905, 10000005, 'Y', NULL, NULL, 'REF126', TO_TIMESTAMP('2023-11-05 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009906, 10000006, 'Y', NULL, NULL, 'REF121', TO_TIMESTAMP('2023-11-06 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009907, 10000007, 'Y', NULL, NULL, NULL, TO_TIMESTAMP('2023-11-07 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009908, 10000008, 'Y', NULL, NULL, NULL, TO_TIMESTAMP('2023-11-08 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009909, 10000009, 'Y', NULL, NULL, 'REF126', TO_TIMESTAMP('2023-11-09 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009910, 10000010, 'Y', NULL, NULL, NULL, TO_TIMESTAMP('2023-11-10 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009911, 10000011, 'Y', NULL, NULL, 'REF134', TO_TIMESTAMP('2023-11-11 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009912, 10000012, 'Y', NULL, NULL, NULL, TO_TIMESTAMP('2023-11-12 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009913, 10000013, 'Y', NULL, NULL, 'REF120', TO_TIMESTAMP('2023-11-13 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009914, 10000014, 'Y', NULL, NULL, NULL, TO_TIMESTAMP('2023-11-14 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009915, 10000015, 'Y', NULL, NULL, 'REF137', TO_TIMESTAMP('2023-11-15 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009916, 10000001, 'Y', NULL, TO_TIMESTAMP('2023-11-01 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-16 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009917, 10000002, 'Y', NULL, TO_TIMESTAMP('2023-11-02 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-17 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009918, 10000003, 'Y', NULL, TO_TIMESTAMP('2023-11-03 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-18 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009919, 10000004, 'Y', NULL, TO_TIMESTAMP('2023-11-04 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-19 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009920, 10000005, 'Y', NULL, TO_TIMESTAMP('2023-11-05 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-20 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009921, 10000006, 'Y', NULL, TO_TIMESTAMP('2023-11-06 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-21 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Shipped');
INSERT INTO C_Order VALUES (3366009922, 10000007, 'Y', NULL, TO_TIMESTAMP('2023-11-07 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-22 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009923, 10000001, NULL, 3366009916, TO_TIMESTAMP('2023-11-16 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-23 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009924, 10000001, NULL, 3366009923, TO_TIMESTAMP('2023-11-23 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-24 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009925, 10000003, NULL, 3366009918, TO_TIMESTAMP('2023-11-18 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-24 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009926, 10000006, NULL, 3366009921, TO_TIMESTAMP('2023-11-21 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-24 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009927, 10000003, NULL, 3366009925, TO_TIMESTAMP('2023-11-24 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-25 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');
INSERT INTO C_Order VALUES (3366009928, 10000001, NULL, 3366009924, TO_TIMESTAMP('2023-11-24 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), NULL, TO_TIMESTAMP('2023-11-25 15:30:45', 'YYYY-MM-DD HH24:MI:SS'), 'Packing');


-- Insert records into ORDERLINE table

INSERT INTO Orderline VALUES (666777888901, 3366009901, 777001, 2);
INSERT INTO Orderline VALUES (666777888902, 3366009902, 777002, 1);
INSERT INTO Orderline VALUES (666777888903, 3366009903, 777003, 3);
INSERT INTO Orderline VALUES (666777888904, 3366009904, 777004, 10);
INSERT INTO Orderline VALUES (666777888905, 3366009905, 777005, 2);
INSERT INTO Orderline VALUES (666777888906, 3366009906, 777006, 1);
INSERT INTO Orderline VALUES (666777888907, 3366009907, 777007, 5);
INSERT INTO Orderline VALUES (666777888908, 3366009908, 777008, 2);
INSERT INTO Orderline VALUES (666777888909, 3366009909, 777009, 4);
INSERT INTO Orderline VALUES (666777888910, 3366009910, 777010, 2);
INSERT INTO Orderline VALUES (666777888911, 3366009911, 777011, 1);
INSERT INTO Orderline VALUES (666777888912, 3366009912, 777012, 7);
INSERT INTO Orderline VALUES (666777888913, 3366009913, 777013, 6);
INSERT INTO Orderline VALUES (666777888914, 3366009914, 777014, 5);
INSERT INTO Orderline VALUES (666777888915, 3366009915, 777015, 3);
INSERT INTO Orderline VALUES (666777888916, 3366009916, 777016, 2);
INSERT INTO Orderline VALUES (666777888917, 3366009917, 777017, 1);
INSERT INTO Orderline VALUES (666777888918, 3366009918, 777018, 3);
INSERT INTO Orderline VALUES (666777888919, 3366009919, 777019, 2);
INSERT INTO Orderline VALUES (666777888920, 3366009920, 777020, 1);
INSERT INTO Orderline VALUES (666777888921, 3366009921, 777001, 3);
INSERT INTO Orderline VALUES (666777888922, 3366009922, 777002, 2);
INSERT INTO Orderline VALUES (666777888923, 3366009901, 777003, 7);
INSERT INTO Orderline VALUES (666777888924, 3366009901, 777006, 6);
INSERT INTO Orderline VALUES (666777888925, 3366009902, 777018, 5);
INSERT INTO Orderline VALUES (666777888926, 3366009903, 777008, 4);
INSERT INTO Orderline VALUES (666777888927, 3366009903, 777016, 3);
INSERT INTO Orderline VALUES (666777888928, 3366009903, 777015, 2);
INSERT INTO Orderline VALUES (666777888929, 3366009904, 777007, 1);
INSERT INTO Orderline VALUES (666777888930, 3366009916, 777007, 10);
INSERT INTO Orderline VALUES (666777888931, 3366009923, 777007, 10);
INSERT INTO Orderline VALUES (666777888932, 3366009924, 777008, 8);
INSERT INTO Orderline VALUES (666777888933, 3366009925, 777016, 20);
INSERT INTO Orderline VALUES (666777888934, 3366009925, 777017, 100);
INSERT INTO Orderline VALUES (666777888935, 3366009926, 777009, 15);
INSERT INTO Orderline VALUES (666777888936, 3366009927, 777005, 10);
INSERT INTO Orderline VALUES (666777888937, 3366009928, 777004, 100);
INSERT INTO Orderline VALUES (666777888938, 3366009928, 777009, 100);


-- Insert records into RATING table

INSERT INTO Rating VALUES (52690001, 10000001, 777001, 5, TO_DATE('2023-11-09', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690002, 10000002, 777002, 4, TO_DATE('2023-11-10', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690003, 10000003, 777003, 3, TO_DATE('2023-11-11', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690004, 10000004, 777004, 2, TO_DATE('2023-11-12', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690005, 10000005, 777005, 2, TO_DATE('2023-11-13', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690006, 10000006, 777006, 1, TO_DATE('2023-11-14', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690007, 10000007, 777007, 5, TO_DATE('2023-11-15', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690008, 10000008, 777008, 4, TO_DATE('2023-11-16', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690009, 10000009, 777009, 3, TO_DATE('2023-11-17', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690010, 10000010, 777010, 2, TO_DATE('2023-11-18', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690011, 10000011, 777011, 1, TO_DATE('2023-11-19', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690012, 10000012, 777012, 5, TO_DATE('2023-11-20', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690013, 10000013, 777013, 4, TO_DATE('2023-11-21', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690014, 10000014, 777014, 3, TO_DATE('2023-11-22', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690015, 10000015, 777015, 2, TO_DATE('2023-11-23', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690016, 10000001, 777016, 1, TO_DATE('2023-11-24', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690017, 10000002, 777018, 1, TO_DATE('2023-11-25', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690018, 10000003, 777018, 1, TO_DATE('2023-11-09', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690019, 10000004, 777007, 5, TO_DATE('2023-11-10', 'YYYY-MM-DD'));
INSERT INTO Rating VALUES (52690020, 10000005, 777020, 4, TO_DATE('2023-11-12', 'YYYY-MM-DD'));

-- Insert records into SHIPMENT table

INSERT INTO Shipment VALUES (6490006661, 3366009901, 'Car', 'DHL', 5.99, 'Packing');
INSERT INTO Shipment VALUES (6490006662, 3366009902, 'Motorcycle', 'Grab', 1.99, 'Awaiting Pickup');
INSERT INTO Shipment VALUES (6490006663, 3366009903, 'Air', 'Fedex Cargo', 50.99, 'Packing');
INSERT INTO Shipment VALUES (6490006664, 3366009904, 'Ship', 'TK', 25.99, 'Packing');
INSERT INTO Shipment VALUES (6490006665, 3366009905, 'Lorry', 'DHL', 10.99, 'Packing');
INSERT INTO Shipment VALUES (6490006666, 3366009906, 'Car', 'DHL', 5.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006667, 3366009907, 'Car', 'DHL', 5.99, 'Packing');
INSERT INTO Shipment VALUES (6490006668, 3366009908, 'Air', 'UPS', 35.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006669, 3366009909, 'Air', 'UPS', 42.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006670, 3366009910, 'Motorcycle', 'Grab', 2.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006671, 3366009911, 'Lorry', 'DHL', 7.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006672, 3366009912, 'Ship', 'TK', 15.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006673, 3366009913, 'Motorcycle', 'Lalamove', 3.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006674, 3366009914, 'Car', 'Lalamove', 3.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006675, 3366009915, 'Air', 'UPS', 20.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006676, 3366009916, 'Air', 'Fedex Cargo', 50.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006677, 3366009917, 'Ship', 'TK', 25.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006678, 3366009918, 'Lorry', 'DHL', 10.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006679, 3366009919, 'Car', 'DHL', 5.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006680, 3366009920, 'Motorcycle', 'Grab', 2.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006681, 3366009921, 'Lorry', 'DHL', 7.99, 'Delivered');
INSERT INTO Shipment VALUES (6490006682, 3366009922, 'Ship', 'TK', 15.99, 'Packing');
INSERT INTO Shipment VALUES (6490006683, 3366009923, 'Car', 'DHL', 5.99, 'Packing');
INSERT INTO Shipment VALUES (6490006684, 3366009924, 'Car', 'DHL', 6.99, 'Packing');
INSERT INTO Shipment VALUES (6490006685, 3366009925, 'Car', 'DHL', 7.99, 'Packing');
INSERT INTO Shipment VALUES (6490006686, 3366009926, 'Car', 'DHL', 3.99, 'Packing');
INSERT INTO Shipment VALUES (6490006687, 3366009927, 'Car', 'DHL', 4.99, 'Packing');
INSERT INTO Shipment VALUES (6490006688, 3366009928, 'Car', 'DHL', 20.99, 'Packing');


-- Insert records into PAYMENT table

INSERT INTO Payment VALUES (48306669991, 3366009901, 'COD');
INSERT INTO Payment VALUES (48306669992, 3366009902, 'COD');
INSERT INTO Payment VALUES (48306669993, 3366009903, 'EWallet');
INSERT INTO Payment VALUES (48306669994, 3366009904, 'Credit Card');
INSERT INTO Payment VALUES (48306669995, 3366009905, 'Credit Card');
INSERT INTO Payment VALUES (48306669996, 3366009906, 'COD');
INSERT INTO Payment VALUES (48306669997, 3366009907, 'EWallet');
INSERT INTO Payment VALUES (48306669998, 3366009908, 'Credit Card');
INSERT INTO Payment VALUES (48306669999, 3366009909, 'EWallet');
INSERT INTO Payment VALUES (48306669910, 3366009910, 'Credit Card');
INSERT INTO Payment VALUES (48306669911, 3366009911, 'COD');
INSERT INTO Payment VALUES (48306669912, 3366009912, 'Credit Card');
INSERT INTO Payment VALUES (48306669913, 3366009913, 'EWallet');
INSERT INTO Payment VALUES (48306669914, 3366009914, 'COD');
INSERT INTO Payment VALUES (48306669915, 3366009915, 'EWallet');
INSERT INTO Payment VALUES (48306669916, 3366009916, 'Credit Card');
INSERT INTO Payment VALUES (48306669917, 3366009917, 'Credit Card');
INSERT INTO Payment VALUES (48306669918, 3366009918, 'COD');
INSERT INTO Payment VALUES (48306669919, 3366009919, 'EWallet');
INSERT INTO Payment VALUES (48306669920, 3366009920, 'Credit Card');
INSERT INTO Payment VALUES (48306669921, 3366009921, 'Credit Card');
INSERT INTO Payment VALUES (48306669922, 3366009922, 'COD');
INSERT INTO Payment VALUES (48306669923, 3366009923, 'COD');
INSERT INTO Payment VALUES (48306669924, 3366009924, 'COD');
INSERT INTO Payment VALUES (48306669925, 3366009925, 'COD');
INSERT INTO Payment VALUES (48306669926, 3366009926, 'COD');
INSERT INTO Payment VALUES (48306669927, 3366009927, 'COD');
INSERT INTO Payment VALUES (48306669928, 3366009928, 'COD');


-- Insert records into ORDER_REVIEW table

INSERT INTO Order_review VALUES (1199000001, 3366009915, 'Great service!', TO_DATE('2023-11-22', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000002, 3366009910, 'Fast delivery!', TO_DATE('2023-11-16', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000003, 3366009912, 'Good quality!', TO_DATE('2023-11-20', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000004, 3366009906, 'Great product!', TO_DATE('2023-11-13', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000005, 3366009908, 'Fast shipping! Awesome!', TO_DATE('2023-11-15', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000006, 3366009909, 'Product come in good quality! Good job!', TO_DATE('2023-11-16', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000007, 3366009911, 'Great herbs product! Recommend for all age.', TO_DATE('2023-11-17', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000008, 3366009913, 'Very fast!', TO_DATE('2023-11-20', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000009, 3366009914, 'Premium product!', TO_DATE('2023-11-21', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000010, 3366009916, 'Awesome! Fast delivery! Good job!', TO_DATE('2023-11-22', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000011, 3366009917, 'Fast shipment!', TO_DATE('2023-11-24', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000012, 3366009918, 'Nice quality!', TO_DATE('2023-11-25', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000013, 3366009919, 'Great product!', TO_DATE('2023-11-26', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000014, 3366009920, 'Fast delivery & great product!', TO_DATE('2023-11-27', 'YYYY-MM-DD'));
INSERT INTO Order_review VALUES (1199000015, 3366009921, 'Good quality!', TO_DATE('2023-11-28', 'YYYY-MM-DD'));



----------------------------------
-------- Indexes Tables ----------
----------------------------------

-- For faster data retrieval when tracking for customer's last name 
CREATE INDEX index_last_name ON CUSTOMER (c_lname);

-- Used for faster data retrieval when tracking for order number,
-- only required to enter last 4 digit of o_id (order id)
CREATE INDEX index_c_order ON C_ORDER (SUBSTR (o_id, -4));

-- Used when tracking for parcel's status
CREATE INDEX index_shipment ON SHIPMENT (s_status);

--  To locate for all "null" or "not null" referred id. 
CREATE INDEX index_referral ON  REFERRAL (referred_c_id);



----------------------------------
---------- View Table ------------
----------------------------------

-- Drop previous created view table

DROP VIEW C_ORDER_TOTAL_PRICE_LOYALTY;
DROP VIEW C_ORDER_LOYALTY;
DROP VIEW C_EXISTING_ORDER;
DROP VIEW C_NEW_REWARD_20;
DROP VIEW C_EXISTING_REWARD_10;
DROP VIEW ORDER_ALL;


-- Total amount for each order, last column represents the 5% credit amount
-- You can say this is a 'before credit' view
CREATE VIEW C_ORDER_TOTAL_PRICE_LOYALTY AS
SELECT
    o.o_id AS order_id,
    o.c_id AS customer_id,
    SUM(ol.ol_quantity * p.p_unit_price) AS total_price,
    CASE
        WHEN o.last_o_date IS NULL THEN 0
        ELSE ROUND(SUM(ol.ol_quantity * p.p_unit_price) * 0.05, 2)  -- Third or above purchase
    END AS loyalty_credit
FROM C_Order o
JOIN Orderline ol ON o.o_id = ol.o_id
JOIN Product p ON ol.p_id = p.p_id
GROUP BY o.o_id, o.c_id, o.last_o_date
ORDER BY o.o_id ASC;


-- Same as "C_ORDER_TOTAL_PRICE_LOYALTY"
CREATE VIEW C_ORDER_LOYALTY AS 
SELECT
    cotpl.order_id,
    o.c_id AS customer_id,
    cotpl.total_price,
    ROUND(cotpl.loyalty_credit, 2) AS new_loyalty_credit
FROM C_ORDER_TOTAL_PRICE_LOYALTY cotpl
JOIN C_Order o ON cotpl.order_id = o.o_id;


-- Customer who has placed two orders beforehand, this is the third time or
-- above he/she places an order, the loyalty_amt will be applied.
-- There is currently only 5 customers who are placing thier third/fourth/etc. order.
CREATE VIEW C_EXISTING_ORDER AS 
SELECT 
    cotpl.order_id AS order_id,
    o.c_id AS customer_id,
    (cotpl.total_price - col.new_loyalty_credit) AS total_payable
    -- ROUND(cotpl.loyalty_credit, 2) AS new_loyalty_credit
FROM C_ORDER_TOTAL_PRICE_LOYALTY cotpl
JOIN C_Order o ON cotpl.order_id = o.o_id
JOIN C_ORDER_LOYALTY col ON o.last_o_id = col.order_id
WHERE o.is_first_and_second_time_purchase IS NULL;


-- New customer who has applied his/her friend's reffaral code will get a 20%
-- discount on their first purchase, this discount will immediately applied
-- to their order before they make payment
-- There is currently 7 customers who have used a referral code
CREATE VIEW C_NEW_REWARD_20 AS 
SELECT 
    cotpl.order_id AS order_id,
    -- cotpl.customer_id AS customer_id,
    ROUND(cotpl.total_price * 0.2, 2) AS new_customer_reward
FROM C_ORDER_TOTAL_PRICE_LOYALTY cotpl
JOIN C_Order o ON cotpl.order_id = o.o_id
WHERE o.referral_code IS NOT NULL AND o.is_first_and_second_time_purchase IS NOT NULL;

-- Referrer who has already refer any amount of friends (not zero) will get
-- a 10% discount on their next purchase. If they didnt make a new order, this
-- reward will be kept until they make their next purchase.
-- Currently, out of the 7 referral, only 5 of the customer made their next purchase
-- as two of them had refer 2 friends instead of 1.
-- Thus, for customer who referred 2 or more friends will get 2 x 10% rewards from
-- their 2 or more friends.
-- The 'customer_referred_id' means the customer id who refer a friend of one or more,
-- not the customer being reffered.
CREATE VIEW C_EXISTING_REWARD_10 AS
SELECT 
    r.referrer_c_id AS customer_referred_id,
    ROUND(SUM(cotpl.total_price * 0.1), 2) AS existing_customer_reward
FROM C_ORDER_TOTAL_PRICE_LOYALTY cotpl
JOIN C_Order o ON cotpl.order_id = o.o_id
JOIN Referral r ON cotpl.customer_id = r.referred_c_id
WHERE o.referral_code IS NOT NULL AND o.is_first_and_second_time_purchase IS NOT NULL
GROUP BY r.referrer_c_id
ORDER BY r.referrer_c_id;


-- Final view table after finish all calculations, including considering any loyalty
-- credit and reward credit that have been applied for each customer.
CREATE VIEW ORDER_ALL AS 
SELECT 
    cotpl.order_id AS order_id, 
    cotpl.customer_id AS customer_id,
    cotpl.total_price AS grand_total_before_credit,
    CASE
        WHEN o.is_first_and_second_time_purchase IS NOT NULL AND cnr20.new_customer_reward IS NULL AND col.new_loyalty_credit = 0 THEN col.total_price --first purchase no referral
        WHEN o.is_first_and_second_time_purchase IS NOT NULL AND cnr20.new_customer_reward IS NOT NULL AND cotpl.order_id = cnr20.order_id THEN ROUND(cotpl.total_price - cnr20.new_customer_reward, 2) --first purchase with referral
        WHEN cotpl.order_id = ceo.order_id THEN ROUND(ceo.total_payable, 2) -- third or more purchase, where reward already used
        WHEN cotpl.customer_id = cer10.customer_referred_id THEN ROUND(cotpl.total_price - cer10.existing_customer_reward, 2) --second or more purchase, with reward 10% credit
        ELSE cotpl.total_price -- second or more purchase , with no reward credit
    END AS grand_total
FROM C_ORDER_TOTAL_PRICE_LOYALTY cotpl
LEFT JOIN C_ORDER_LOYALTY col ON cotpl.order_id = col.order_id
LEFT JOIN C_EXISTING_ORDER ceo ON cotpl.order_id = ceo.order_id
LEFT JOIN C_NEW_REWARD_20 cnr20 ON cotpl.order_id = cnr20.order_id
JOIN C_Order o ON cotpl.order_id = o.o_id
LEFT JOIN C_EXISTING_REWARD_10 cer10 ON cotpl.customer_id = cer10.customer_referred_id
ORDER BY order_id;